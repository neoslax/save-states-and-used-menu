package ru.eltech.activityandlayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import ru.eltech.activityandlayouts.databinding.ActivityLiveDataBinding

class LiveDataActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityLiveDataBinding.inflate(layoutInflater)
    }

    private val viewModel by lazy {
        ViewModelProvider(this)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        viewModel.editTextState.observe(this) {
            binding.etLd.setText(it)
            Toast.makeText(this, "Data in LD: $it", Toast.LENGTH_SHORT).show()
        }

        binding.btnLdActivity.setOnClickListener {
            viewModel.saveStringInLD(binding.etLd.text.toString())
        }

    }
}