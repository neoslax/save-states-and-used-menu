package ru.eltech.activityandlayouts

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import ru.eltech.activityandlayouts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        savedInstanceState?.let {
            restoreData(it)
        }

        binding.btnProceed.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java).apply {
                putExtras(getStateBundle())
            }
            startActivity(intent)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putAll(getStateBundle())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_save -> saveData()
            R.id.menu_restore_state -> loadData()
            R.id.menu_change_bg -> setRandomPicOnBg()
            R.id.menu_go_to_ld -> {
                startActivity(
                    Intent(
                        this,
                        LiveDataActivity::class.java
                    )
                )
            }
        }
        return true
    }

    private fun setRandomPicOnBg() {
        val images = listOf<Int>(
            R.drawable.img1,
            R.drawable.img2,
            R.drawable.img3,
            R.drawable.img4,
            R.drawable.img5,
            R.drawable.img6
        )
        val picNum = (Math.random() * images.size).toInt()
        binding.clMain.background = AppCompatResources.getDrawable(this, images[picNum])
    }

    private fun getStateBundle(): Bundle {
        return Bundle().apply {
            putString("lName", binding.etLastName.text.toString())
            putString("name", binding.etName.text.toString())
            putString("sName", binding.etSecondName.text.toString())
            putString("age", binding.etAge.text.toString())
            putString("hobbies", binding.etHobbies.text.toString())
        }
    }

    private fun restoreData(bundle: Bundle) {
        with(binding) {
            etLastName.setText(bundle.getString("lName", ""))
            etName.setText(bundle.getString("name", ""))
            etSecondName.setText(bundle.getString("sName", ""))
            etAge.setText(bundle.getString("age", ""))
            etHobbies.setText(bundle.getString("hobbies", ""))
        }
    }

    private fun saveData() {
        val preferences = getPreferences(Context.MODE_PRIVATE)
        preferences.edit().apply {
            putString("lName", binding.etLastName.text.toString())
            putString("name", binding.etName.text.toString())
            putString("sName", binding.etSecondName.text.toString())
            putString("age", binding.etAge.text.toString())
            putString("hobbies", binding.etHobbies.text.toString())
        }.apply()
        Toast.makeText(this, "Saved in shared", Toast.LENGTH_SHORT).show()
    }

    private fun loadData() {
        val preferences = getPreferences(Context.MODE_PRIVATE)
        with(binding) {
            etLastName.setText(preferences.getString("lName", ""))
            etName.setText(preferences.getString("name", ""))
            etSecondName.setText(preferences.getString("sName", ""))
            etAge.setText(preferences.getString("age", ""))
            etHobbies.setText(preferences.getString("hobbies", ""))
        }
        Toast.makeText(this, "Loaded from shared", Toast.LENGTH_SHORT).show()
    }

}