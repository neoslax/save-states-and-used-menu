package ru.eltech.activityandlayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.eltech.activityandlayouts.databinding.InfoActivityBinding

class InfoActivity : AppCompatActivity() {

    private val binding by lazy {
        InfoActivityBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        intent.extras?.let {
            with(it) {
                binding.tvResult.text = "Фамилия: ${getString("lName")} \n" +
                        "Имя: ${getString("name")} \n"+
                        "Отчество: ${getString("sName")} \n"+
                        "Возраст: ${getString("age")} \n"+
                        "Хобби: ${getString("hobbies")} \n"
            }
        }

    }
}