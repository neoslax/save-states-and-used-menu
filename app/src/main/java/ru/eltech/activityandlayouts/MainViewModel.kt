package ru.eltech.activityandlayouts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    private val _editTextState = MutableLiveData<String>()
    val editTextState: LiveData<String>
        get() = _editTextState

    fun saveStringInLD(str: String) = _editTextState.postValue(str)
}